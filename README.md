# Infinity Scheduler
Ever feel like there's not enough time in the day to do the things you want to do?
Infinity Scheduler assists you in discovering your optimal schedule.
Able to determine, adapt and improve your scheduling habits, Infinity Scheduler curates your time so that you can do more of the things you love to do. 
Infinity Scheduler learns your time usage and seemlessly enchances your use of time.
This digital executive assistant will even reschedule missed appointments in availble timeslots.
Check it out at http://infinityscheduler.com/

# Launching The Project
Infinity Scheduler requires xampp for local development. You get xampp here: https://www.apachefriends.org/index.html. Once xampp is installed, clone this repository into 
xampp/htdocs/. Then, make sure xampp is running and navigate to xampp/htdocs/Infinity-Scheduler/Infinity-Scheduler/ and run "npm run devbuild". This will create a build of
the vueJS frontend and copy the php server files into xampp/htdocs. The site should be running at localhost at this point. "npm run devbuild" will have to be run after
each update is made in order to see any changes made.

# Features To Add
-Cross scheduling referencing

-Location tracking

-Automatic event completetion detection

-Resource tracking

-Custom Categories

-Automatic categorization based on event title

